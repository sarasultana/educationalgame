package com.example.educationalgame;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class ArabicActivity extends AppCompatActivity
{
    ImageView image_view;
    Button start_button;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arabic);
        start_button = findViewById(R.id.start_button);
        image_view = findViewById(R.id.image_btn);
        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                start_button.setText("Next");
                int[] src = new int[6];
                src[0] = R.drawable.page_1;
                src[1] = R.drawable.page_2;
                src[2] = R.drawable.page_3;
                src[3] = R.drawable.page_4;
                src[4] = R.drawable.page_5;
                src[5] = R.drawable.page_6;
                image_view.setImageDrawable(getResources().getDrawable(src[count]));
                count++;
                if(count == 5)
                {
                    Intent intent = new Intent(ArabicActivity.this, MainActivity.class);
                    startActivity(intent);
                }

            }
        });

        }

   }
