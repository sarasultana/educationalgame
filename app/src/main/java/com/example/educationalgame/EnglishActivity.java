package com.example.educationalgame;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class EnglishActivity extends AppCompatActivity {
    TextView ques;
    EditText ans;
    ArrayList<String> questions = new ArrayList<String>();
    ArrayList<String> answers = new ArrayList<String>();
    int count = 0;
    Button chkbutton;
    long total_time;
    long min;
    long sec;
    Chronometer timer;
    int millis;
    public static String eng_totaltime;
  //  SharedPreferences sharedPref;
  //  SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_english);

        timer = (Chronometer) findViewById(R.id.timer);
        ques = (TextView) findViewById(R.id.ques);
        ans = (EditText) findViewById(R.id.ans);
        Button myButton = findViewById(R.id.Button_start);
        chkbutton = findViewById(R.id.chk_button);
        Button homebutton = findViewById(R.id.Button_home);
        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getQuestions();
            }
        });
        chkbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                checkAnswers();
            }
        });
        homebutton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(EnglishActivity.this, MainActivity.class);
        startActivity(intent);
    }
});

    }

    private void getQuestions() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = "https://sheets.googleapis.com/v4/spreadsheets/1wut72WqbCv3bAbVGzFaDTYVVf_AJvQAmoVeXrXl81e8/values/English!A1:B10/?key=AIzaSyAnNldZWa3VZLO13EmyVNK4dPVG-Hyv_VI";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("Debug", response);
                parseItems(response);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        int socketTimeOut = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeOut, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }

    private void parseItems(String jsonResponse) {
        try {
            JSONObject jobj = new JSONObject(jsonResponse);
            JSONArray jarray = jobj.getJSONArray("values");

            ArrayList<String> listdata = new ArrayList<String>();

            if (jarray != null) {
                for (int i = 0; i < jarray.length(); i++) {
                    listdata.add(jarray.getString(i));
                    String[] separated = listdata.get(i).split(",");
                    questions.add(i, separated[0].substring(2).replace("\"", ""));
                    answers.add(i, separated[1].replace("]", "").replaceAll("\"", ""));
                }

                ques.setText(questions.get(count));
                ans.setVisibility(View.VISIBLE);
                ans.setShowSoftInputOnFocus(true);
                chkbutton.setVisibility(View.VISIBLE);
              // if(sharedPref != null)
               //     editor.remove("english_time");

                timer.start();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkAnswers() {
        String getans = ans.getText().toString();
        if (getans.contentEquals(answers.get(count)))
        {
            //Toast.makeText(getApplicationContext(), "Good Job Ammar !", Toast.LENGTH_SHORT);
            Toast toast= Toast.makeText(getApplicationContext(),
                    "Good Job Ammar !", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            count++;
            if (count == questions.size()) {
                timer.stop();
                total_time = timer.getBase();
                millis = (int) (SystemClock.elapsedRealtime() - timer.getBase());
                min = TimeUnit.MILLISECONDS.toMinutes(millis);
                sec = TimeUnit.MILLISECONDS.toSeconds(millis);
                eng_totaltime = min + " Minutes and " + sec + " Seconds";
               // sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
              //  editor = sharedPref.edit();
              //  editor.putString("english_time", totaltime);
              //  editor.apply();

                LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                View customView = inflater.inflate(R.layout.popup_layout, null);
                PopupWindow popW = new PopupWindow(customView, 1000, 1000);
                Button backbtn = (Button) customView.findViewById(R.id.back_button);
                backbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(EnglishActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                popW.showAtLocation(findViewById(R.id.linearLayout), Gravity.CENTER, 0, 0);

            } else {
                ans.setText("");
                ques.setText(questions.get(count));
            }
        } else
        {
            //Toast.makeText(getApplicationContext(), "Try Again Ammar !", Toast.LENGTH_SHORT).show();
            Toast toast= Toast.makeText(getApplicationContext(),
                    "Try Again Ammar !", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count",count);
        outState.putStringArrayList("questions",questions);
        outState.putStringArrayList("answers",answers);
        outState.putLong("timer",timer.getBase());

    }
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {

        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("count");
        questions=savedInstanceState.getStringArrayList("questions");
        answers=savedInstanceState.getStringArrayList("answers");
        long time_saved = savedInstanceState.getLong("timer");
        timer.setBase(time_saved);
        timer.start();
        ques.setText(questions.get(count));
        ans.setVisibility(View.VISIBLE);
        ans.setShowSoftInputOnFocus(true);
        chkbutton.setVisibility(View.VISIBLE);
    }

}
