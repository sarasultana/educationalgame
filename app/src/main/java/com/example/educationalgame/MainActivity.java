package com.example.educationalgame;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity<onClickListener> extends AppCompatActivity implements View.OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton button1 = (ImageButton)findViewById(R.id.Button_00);
        button1.setOnClickListener(this);
        ImageButton button2 = (ImageButton)findViewById(R.id.Button_01);
        button2.setOnClickListener(this);
        ImageButton button3 = (ImageButton)findViewById(R.id.Button_10);
        button3.setOnClickListener(this);
        ImageButton button4 = (ImageButton)findViewById(R.id.Button_11);
        button4.setOnClickListener(this);
        ImageButton button5 = (ImageButton)findViewById(R.id.Button_20);
        button5.setOnClickListener(this);
        ImageButton button6 = (ImageButton)findViewById(R.id.Button_21);
        button6.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.Button_00:
                Intent intent_math = new Intent(this, MathActivity.class);
                v.getContext().startActivity(intent_math);
                break;

            case R.id.Button_01:
                Intent intent_english=new Intent(this, EnglishActivity.class);
                v.getContext().startActivity(intent_english);
                break;
            case R.id.Button_10:
                Intent intent_science = new Intent(this,ScienceActivity.class);
                v.getContext().startActivity(intent_science);
                break;
            case R.id.Button_11:
                Intent intent_SS =new Intent(this, SSActivity.class);
                v.getContext().startActivity(intent_SS);
                break;
            case R.id.Button_20:
                Intent intent_Arabic = new Intent(this,ArabicActivity.class);
                v.getContext().startActivity(intent_Arabic);
                break;
            case R.id.Button_21:
                Intent intent_Report = new Intent(this, ReportActivity.class);
                v.getContext().startActivity(intent_Report);
                break;

        }
    }
}
