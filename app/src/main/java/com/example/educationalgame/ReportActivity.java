package com.example.educationalgame;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ReportActivity extends AppCompatActivity
{

    TextView math;
    TextView science;
    TextView english;
    TextView ss;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Button show = findViewById(R.id.show_btn);
        math = findViewById(R.id.math);
        science = findViewById(R.id.science);
        english = findViewById(R.id.english);
        ss = findViewById(R.id.socialstudies);

        show.setOnClickListener(new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

         /*   SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String math_time = sharedPref.getString("math_time", "Not Available");
            String science_time = sharedPref.getString("science_time", "Not Available");
            String english_time = sharedPref.getString("english_time", "Not Available");
            String ss_time = sharedPref.getString("ss_time", "Not Available");

          */

            math.setVisibility(View.VISIBLE);
            science.setVisibility(View.VISIBLE);
            english.setVisibility(View.VISIBLE);
            ss.setVisibility(View.VISIBLE);

            if(MathActivity.math_totaltime == null) {
                math.setText("Math Activity not completed !");
                math.setTextColor(getResources().getColor(R.color.report_text));
            }

            else
                math.setText("Math Excercise completed in : " + MathActivity.math_totaltime);

            if(ScienceActivity.science_totaltime == null) {
                science.setText("Science Activity not completed !");
                science.setTextColor(getResources().getColor(R.color.report_text));
            }
            else
                science.setText("Science Excercise completed in : " + ScienceActivity.science_totaltime);

            if(EnglishActivity.eng_totaltime == null){
                english.setText("English Activity not completed !");
                english.setTextColor(getResources().getColor(R.color.report_text));}
            else
                english.setText("English Excercise completed in : " + EnglishActivity.eng_totaltime);

            if(SSActivity.ss_totaltime == null){
                ss.setText("Social Studies Activity not completed !");
                ss.setTextColor(getResources().getColor(R.color.report_text));}
            else
                ss.setText("Social Studies Excercise completed in : " + SSActivity.ss_totaltime);

        }
    });

    }
}
