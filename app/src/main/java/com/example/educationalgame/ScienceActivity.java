package com.example.educationalgame;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

public class ScienceActivity extends AppCompatActivity {
    TextView ques;
    EditText ans;
    RadioButton op1;
    RadioButton op2;
    Button chkbutton;
    ArrayList<String> questions = new ArrayList<String>();
    ArrayList<String> option1 = new ArrayList<>();
    ArrayList<String> option2 = new ArrayList<>();
    ArrayList<String> answers = new ArrayList<String>();
    int count = 0;
    RadioGroup radioGroup;
    long total_time;
    long min;
    long sec;
    Chronometer timer;
    int millis;
    public static String science_totaltime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_science);

        timer = (Chronometer) findViewById(R.id.timer);
        radioGroup = (RadioGroup) findViewById(R.id.radio_ans);
        ques = (TextView) findViewById(R.id.ques);
        op1 = (RadioButton) findViewById(R.id.rd_1) ;
        op2 = (RadioButton) findViewById(R.id.rd_2);
        Button myButton = findViewById(R.id.Button_start);
        chkbutton = findViewById(R.id.chk_button);
        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getQuestions();
            }
        });
        chkbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                checkAnswers();
            }
        });
        Button homebutton = findViewById(R.id.Button_home);
        homebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScienceActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

    private void getQuestions() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url = "https://sheets.googleapis.com/v4/spreadsheets/1wut72WqbCv3bAbVGzFaDTYVVf_AJvQAmoVeXrXl81e8/values/Science!A1:D10/?key=AIzaSyAnNldZWa3VZLO13EmyVNK4dPVG-Hyv_VI";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("Debug", response);
                parseItems(response);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        int socketTimeOut = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeOut, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    private void parseItems(String jsonResponse) {
        try {
            JSONObject jobj = new JSONObject(jsonResponse);
            JSONArray jarray = jobj.getJSONArray("values");
            ArrayList<String> listdata = new ArrayList<String>();
            StringTokenizer separated;

            if (jarray != null) {
                for (int i = 0; i < jarray.length(); i++)
                {
                    listdata.add(jarray.getString(i));
                    separated = new StringTokenizer(listdata.get(i), ",");
                    while(separated.hasMoreElements())
                    {
                        questions.add(separated.nextToken().replace("[","").replaceAll("\"",""));
                        option1.add(separated.nextToken().replaceAll("\"",""));
                        option2.add(separated.nextToken().replaceAll("\"",""));
                        answers.add(separated.nextToken().replace("]","").replaceAll("\"",""));
                    }

                }
                ques.setText(questions.get(count));
                radioGroup.setVisibility(View.VISIBLE);
                op1.setText(option1.get(count));
                op2.setText(option2.get(count));
                chkbutton.setVisibility(View.VISIBLE);
                timer.start();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkAnswers() {

        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton)radioGroup.findViewById(radioButtonID);
        radioButton.setChecked(true);
        String getans = (String) radioButton.getText();

        if (getans.contentEquals(answers.get(count)))
        {
            Toast toast= Toast.makeText(getApplicationContext(),
                    "Good Job Ammar !", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            count++;
            if (count == questions.size()) {
                timer.stop();
                total_time = timer.getBase();
                millis = (int) (SystemClock.elapsedRealtime() - timer.getBase());
                min = TimeUnit.MILLISECONDS.toMinutes(millis);
                sec = TimeUnit.MILLISECONDS.toSeconds(millis);
                science_totaltime = min + " Minutes and " + sec + " Seconds";
              /*  SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("science_time", totaltime);
                editor.commit();

               */

                LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                View customView = inflater.inflate(R.layout.popup_layout, null);
                PopupWindow popW = new PopupWindow(customView, 1000, 1000);
                Button backbtn = (Button) customView.findViewById(R.id.back_button);
                backbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ScienceActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                popW.showAtLocation(findViewById(R.id.linearLayout), Gravity.CENTER, 0, 0);

            }
            else {
                    radioGroup.clearCheck();
                    ques.setText(questions.get(count));
                    op1.setText(option1.get(count));
                    op2.setText(option2.get(count));
                   }
        }
        else
        {
            Toast toast= Toast.makeText(getApplicationContext(),
                    "Try Again Ammar !", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count",count);
        outState.putStringArrayList("questions",questions);
        outState.putStringArrayList("answers",answers);
        outState.putStringArrayList("option1",option1);
        outState.putStringArrayList("option2",option2);
        outState.putLong("timer",timer.getBase());

    }
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {

        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("count");
        questions=savedInstanceState.getStringArrayList("questions");
        answers=savedInstanceState.getStringArrayList("answers");
        option1=savedInstanceState.getStringArrayList("option1");
        option2=savedInstanceState.getStringArrayList("option2");
        long time_saved = savedInstanceState.getLong("timer");
        timer.setBase(time_saved);
        timer.start();
        ques.setText(questions.get(count));
        radioGroup.setVisibility(View.VISIBLE);
        op1.setText(option1.get(count));
        op1.setChecked(false);
        op2.setText(option2.get(count));
        op2.setChecked(false);
        chkbutton.setVisibility(View.VISIBLE);
    }

}
